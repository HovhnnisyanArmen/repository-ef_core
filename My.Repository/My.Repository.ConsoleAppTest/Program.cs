﻿using My.Repository.Implementation;
using My.Repository.Interfaces;
using My.Repository.Models;
using System;
using System.Linq;

namespace My.Repository.ConsoleAppTest
{
    class Program
    {
        static void Main(string[] args)
        {
            using (MyDBContext context = new MyDBContext())
            {
                StudentRepository st = new StudentRepository(context);
                var list = st.GetAll().ToList();

                foreach (var item in list)
                {
                    Console.WriteLine($"{item.FirstName} {item.LastName} ");
                }



                IUnitOfWork unitOfWork= new UnitOfWork_(context);

                var teachers = unitOfWork.Teachers.GetAll().ToList();
                foreach (var item in teachers)
                {
                    Console.WriteLine($"{item.FirstName} {item.LastName} ");
                }
            }


        }  
    }
}
