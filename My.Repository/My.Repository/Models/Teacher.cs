﻿using System;
using System.Collections.Generic;
using System.Text;

namespace My.Repository.Models
{
    public class Teacher
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
        public Gender Gender { get; set; }
        public Universite Universite { get; set; }
    }
}
