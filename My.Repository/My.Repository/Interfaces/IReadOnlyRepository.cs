﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace My.Repository.Interfaces
{
    public interface IReadOnlyRepository<TEntity>
        where TEntity:class,new()
    {
        bool Any(TEntity entity);
        bool Any(Expression<Func<TEntity, bool>> predicate);
        IEnumerable<TEntity> GetAll();
        IEnumerable<TEntity> GetAll(Expression<Func<TEntity, bool>> predicate);
        TEntity GetById(int id);
    }
}
