﻿using System;
using System.Collections.Generic;
using System.Text;

namespace My.Repository.Interfaces
{
    public interface IBaseReopsitory<TEntity> : IActionRepository<TEntity>,IReadOnlyRepository<TEntity>
        where TEntity: class, new()
    {

    }
}
