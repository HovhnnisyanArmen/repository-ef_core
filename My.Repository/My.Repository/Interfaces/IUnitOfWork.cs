﻿using System;
using System.Collections.Generic;
using System.Text;

namespace My.Repository.Interfaces
{
    public interface IUnitOfWork
    {
        IStudentRepository Students { get; }
        ITeacherRepository Teachers { get; }
        IUniversiteRepository Universities { get; }
        IGenderRepository Genders { get; }
        void Commit();
        void RejectChanges();
        void Dispose();
    }
}
