﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using My.Repository.Models;
using System;

namespace My.Repository
{
    public class MyDBContext: DbContext
    {

        public DbSet<Student> Students { get; set; }
        public DbSet<Teacher> Teachers { get; set; }
        public DbSet<Universite> Universites { get; set; }
        public DbSet<Gender> Gender { get; set; }


        public MyDBContext()
        {
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var configuration = new ConfigurationBuilder()
                .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
                .AddJsonFile("appsettings.json")
                .Build();
            optionsBuilder.UseSqlServer(configuration.GetConnectionString("DefaultConnection"));
        }
    }
}
