﻿using Microsoft.EntityFrameworkCore;
using My.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace My.Repository
{
    public class BaseRepository<TEntity> : IBaseReopsitory<TEntity>
                    where TEntity:class,new()
    {
        protected readonly DbContext _context;
        protected BaseRepository(DbContext dbContext)
        {
            _context = dbContext;
        }

        public bool Any(TEntity entity)
        {
            if (entity != null)
            {
                return _context.Set<TEntity>().Any(p => p == entity);
            }
            return false;
        }

        public bool Any(Expression<Func<TEntity, bool>> predicate)
        {
            return _context.Set<TEntity>().Any(predicate);
        }

        public void Create(TEntity entity)
        {
            if (entity != null)
            {
                _context.Set<TEntity>().Add(entity);
            }
        }

        public void CreateMany(IEnumerable<TEntity> entities)
        {
            if (entities != null)
            {
                _context.Set<TEntity>().AddRange(entities);
            }
        }

        public void Delete(TEntity entity)
        {
            if (entity != null)
            {
                _context.Set<TEntity>().Remove(entity);
            }
        }

        public void DeleteById(int id)
        {
            var item = GetById(id);
            Delete(item);
        }

        public void DeleteRange(IEnumerable<TEntity> entities)
        {
            if (entities != null)
            {
                _context.Set<TEntity>().RemoveRange(entities);
            }
        }

        public IEnumerable<TEntity> GetAll()
        {
            return _context.Set<TEntity>();
        }

        public IEnumerable<TEntity> GetAll(Expression<Func<TEntity, bool>> predicate)
        {
            return _context.Set<TEntity>().Where(predicate);
        }

        public TEntity GetById(int id)
        {
            return _context.Set<TEntity>().Find(id);
        }

        public void Update(TEntity entity)
        {
            if (entity != null)
            {
                _context.Set<TEntity>().Update(entity);
            }
        }

        public void UpdateRange(IEnumerable<TEntity> entities)
        {
            if (entities != null)
            {
                _context.Set<TEntity>().UpdateRange(entities);
            }
        }

       
    }
}
