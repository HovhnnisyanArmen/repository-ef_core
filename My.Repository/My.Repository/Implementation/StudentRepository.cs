﻿using Microsoft.EntityFrameworkCore;
using My.Repository.Interfaces;
using My.Repository.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace My.Repository.Implementation
{
   public class StudentRepository :BaseRepository<Student>, IStudentRepository
    {
        public StudentRepository(DbContext dbContext):base(dbContext)
        {

        }
    }
}
