﻿using Microsoft.EntityFrameworkCore;
using My.Repository.Interfaces;
using My.Repository.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace My.Repository.Implementation
{
    public class GenderRepository :BaseRepository<Gender>,IGenderRepository
    {
        public GenderRepository(DbContext dBContext):base(dBContext)
        {

        }
    }
}
