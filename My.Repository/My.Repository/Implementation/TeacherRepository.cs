﻿using Microsoft.EntityFrameworkCore;
using My.Repository.Interfaces;
using My.Repository.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace My.Repository.Implementation
{
    public class TeacherRepository : BaseRepository<Teacher>, ITeacherRepository

    {
        public TeacherRepository(DbContext dbContext) : base(dbContext)
        {

        }
    }
}
