﻿using Microsoft.EntityFrameworkCore;
using My.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace My.Repository.Implementation
{
    public class UnitOfWork : IUnitOfWork
    {
        private DbContext _dbContext;

        public IStudentRepository Students { get; private set;}

        public ITeacherRepository Teachers { get; private set; }

        public IUniversiteRepository Universities { get; private set; }

        public IGenderRepository Genders { get; private set; }

        public UnitOfWork(DbContext dbContext)
        {
            _dbContext = dbContext;
            Genders = new GenderRepository(_dbContext);
            Students = new StudentRepository(_dbContext);
            Teachers = new TeacherRepository(_dbContext);
            Universities = new UniversiteRepository(_dbContext);
        }

        public void Commit()
        {
            _dbContext.SaveChanges();
        }

        public void Dispose()
        {
            if (_dbContext != null)
                _dbContext.Dispose();
        }

        public void RejectChanges()
        {
            foreach (var entry in _dbContext.ChangeTracker.Entries()
                  .Where(e => e.State != EntityState.Unchanged))
            {
                switch (entry.State)
                {
                    case EntityState.Added:
                        entry.State = EntityState.Detached;
                        break;
                    case EntityState.Modified:
                    case EntityState.Deleted:
                        entry.Reload();
                        break;
                }
            }
        }
    }
}
