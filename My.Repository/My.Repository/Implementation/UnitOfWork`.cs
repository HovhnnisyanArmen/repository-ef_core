﻿using Microsoft.EntityFrameworkCore;
using My.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace My.Repository.Implementation
{
    public class UnitOfWork_ : IDisposable, IUnitOfWork
    {
        private readonly DbContext _dbContext;
        private bool disposed;
        private readonly Dictionary<string, object> repositories;

        public UnitOfWork_(DbContext dbContext)
        {
            _dbContext = dbContext;
            repositories = new Dictionary<string, object>();
        }

        private TRepository Repository<TRepository>()
        //where TRepository : IBaseRepository<>
        //where T : class, new()
        {
            var type = typeof(TRepository);
            if (!repositories.ContainsKey(type.Name))
            {
                var obj = Activator.CreateInstance(type, _dbContext);
                repositories.Add(type.Name, obj);
            }
            return (TRepository)repositories[type.Name];
        }

        public IStudentRepository Students => Repository<StudentRepository>();

        public ITeacherRepository Teachers => Repository<TeacherRepository>();

        public IUniversiteRepository Universities => Repository<UniversiteRepository>();

        public IGenderRepository Genders => Repository<GenderRepository>();

        public void Commit()
        {
            _dbContext.SaveChanges();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _dbContext.Dispose();
                }
            }
            disposed = true;
        }

        public void RejectChanges()
        {
            foreach (var entry in _dbContext.ChangeTracker.Entries()
                  .Where(e => e.State != EntityState.Unchanged))
            {
                switch (entry.State)
                {
                    case EntityState.Added:
                        entry.State = EntityState.Detached;
                        break;
                    case EntityState.Modified:
                    case EntityState.Deleted:
                        entry.Reload();
                        break;
                }
            }
        }
    }
}
