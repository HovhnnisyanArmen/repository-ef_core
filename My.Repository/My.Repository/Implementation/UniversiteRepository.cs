﻿using Microsoft.EntityFrameworkCore;
using My.Repository.Interfaces;
using My.Repository.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace My.Repository.Implementation
{
   public class UniversiteRepository : BaseRepository<Universite>, IUniversiteRepository
    {
        public UniversiteRepository(DbContext dbContext) : base(dbContext)
        {

        }
    }
}
